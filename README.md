# Focus Blur plug-in version 3.2.7, Feb 2016
----------------------------------------------------------------

### Description


Focus Blur plug-in is blurring effect, a kind of called DoF.
This software makes a out of focus with luminosity and depth,
like a sight or lenses.
It can be used with depth map, depth fakes and shine effect.
Also it can work as simple and applicable blur.

### License

This package is distributed under the GPL. see also COPYING.



Kyoichiro Suda <das atmark dream dot japan>

fix by Alexander Gerber 

### Compile


>  % ./configure

> % make -lm

> % sudo make install 


### Notice 

The Options there are in INSTALL are untested unter newer versions


